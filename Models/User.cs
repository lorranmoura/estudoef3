using System.ComponentModel.DataAnnotations;

namespace ApiEstudoEF3.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Role { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [MaxLength(20, ErrorMessage = "Este campo deve conter entre 5 e 60 caracteres")]
        [MinLength(3, ErrorMessage = "Este campo deve conter entre 5 e 60 caracteres")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [MaxLength(20, ErrorMessage = "Este campo deve conter entre 5 e 60 caracteres")]
        [MinLength(3, ErrorMessage = "Este campo deve conter entre 5 e 60 caracteres")]
        public string Username { get; set; }

    }
}