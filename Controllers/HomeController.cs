using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiEstudoEF3.Data;
using ApiEstudoEF3.Models;

namespace ApiEstudoEF3.Controllers
{
    [Route("v1")]
    public class HomeController : Controller
    {
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<dynamic>> Get([FromServices] DataContext context)
        {
            var employee = new User { Id = 1, Username = "Loran", Password = "123456", Role = "employee" };
            var manager = new User { Id = 2, Username = "Teste", Password = "123456", Role = "manager" };
            var category = new Category { Id = 1, Title = "Informática" };
            var product = new Product { Id = 1, Category = category, Title = "Chinelo", Price = 30, Description = "Chinelo Knef" };
            context.Users.Add(employee);
            context.Users.Add(manager);
            context.Categories.Add(category);
            context.Products.Add(product);
            await context.SaveChangesAsync();

            return Ok(new
            {
                message = "Dados configurados"
            });
        }
    }
}